from client import CoinbaseClient, Price, PricingType

try:
    with CoinbaseClient('https://api.commerce.coinbase.com', '') as c:
        # result = c.charges.create('Test Charge', PricingType.FIXED_PRICE, Price('USD', 1), customer_id='test_customer_id')
        # result = c.charges.list()
        # result = c.charges.find('e68f7982-8526-4ca2-832e-4067ed85988c')

        # result = c.checkouts.create('Test checkout', PricingType.FIXED_PRICE, Price('USD', 5), description='test checkout')
        # result = c.checkouts.list()
        # result = c.checkouts.update('71793480-05fc-498c-86e5-ceac0e51aa5d', 'Test checkout', PricingType.FIXED_PRICE,
        #                             Price('USD', 10), description='Updated checkout')
        # result = c.checkouts.find('71793480-05fc-498c-86e5-ceac0e51aa5d')
        result = c.checkouts.delete('71793480-05fc-498c-86e5-ceac0e51aa5d')
        print(result)
except Exception as err:
    print(str(err))