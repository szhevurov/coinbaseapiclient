from __future__ import annotations
from typing import Optional, Type
from types import TracebackType
from httpx import Client, Response

from .api_groups import ChargesGroup, CheckoutsGroup

API_KEY_HEADER_NAME = "X-CC-Api-Key"
API_VERSION_HEADER_NAME = "X-CC-Version"
API_VERSION = "2018-03-22"


class CoinbaseClient:
    def __init__(self, host_url: str, api_key: str) -> None:
        self.__validate(host_url, api_key)

        self.__client = Client(
            base_url=host_url,
            headers={
                API_KEY_HEADER_NAME: api_key,
                API_VERSION_HEADER_NAME: API_VERSION,
            },
            event_hooks={"response": [Response.raise_for_status]},
        )

        self.charges = ChargesGroup(self.__client)
        self.checkouts = CheckoutsGroup(self.__client)

    def __enter__(self) -> CoinbaseClient:
        self.__client.__enter__()
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]] = None,
        exc_value: Optional[BaseException] = None,
        traceback: Optional[TracebackType] = None,
    ) -> None:
        self.__client.__exit__(exc_type, exc_value, traceback)

    def __validate(self, host_url, api_key) -> None:
        if not host_url:
            raise ValueError(f"Invalid API url: {host_url}")

        if not api_key:
            raise ValueError(f"Invalid API key: {api_key}")
