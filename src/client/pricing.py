from enum import Enum

class PricingType(Enum):
    NO_PRICE, FIXED_PRICE = range(2)

class Price:
    def __init__(self, currency: str, amount: float):
        self.__currency = currency
        self.__amount = amount

    @property
    def currency(self):
        return self.__currency
    
    @property
    def amount(self):
        return self.__amount