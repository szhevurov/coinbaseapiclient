from typing import Optional
from httpx import Client

from .pricing import PricingType, Price


class ApiGroup:
    def __init__(self, name: str, client: Client):
        self.__validate(name, client)

        self._name = name
        self._client = client

    def _path(self, *args: str) -> str:
        return "/".join([self._name, *args])

    def __validate(self, name: str, client: Client) -> None:
        if not name:
            raise ValueError(f"Invalid group name: {name}")

        if not client:
            raise ValueError("Invalid API client")


class ChargesGroup(ApiGroup):
    def __init__(self, client: Client):
        super().__init__("charges", client)

    def list(
        self,
        *,
        limit: Optional[int] = None,
        starting_after: Optional[str] = None,
        ending_before: Optional[str] = None,
    ):
        response = self._client.get(
            self._path(),
            params={
                "limit": limit,
                "starting_after": starting_after,
                "ending_before": ending_before,
            },
        )

        return response.json()

    def find(self, charge_id: str):
        return self._client.get(self._path(charge_id)).json()

    def cancel(self, charge_id: str):
        return self._client.post(self._path(charge_id, "cancel")).json()

    def resolve(self, charge_id: str):
        """Resolve a charge that has been previously marked as unresolved. Supply the unique charge code that was
        returned when the charge was created. Note: Only unresolved charges can be successfully resolved. For more on
        unresolved charges, visit the Charge section of this reference. """
        return self._client.post(self._path(charge_id, "resolve")).json()

    def create(
        self,
        name: str,
        pricing_type: PricingType,
        local_price: Price,
        *,
        description: str = None,
        redirect_url: str = None,
        cancel_url: str = None,
        customer_id: str = None,
    ):
        payload = {
            "name": name,
            "description": description,
            "pricing_type": pricing_type.name.lower(),
            "local_price": {
                "currency": local_price.currency,
                "amount": local_price.amount,
            },
            "redirect_url": redirect_url,
            "cancel_url": cancel_url,
            "metadata": {"customer_id": customer_id},
        }

        return self._client.post(self._path(), json=payload).json()


class CheckoutsGroup(ApiGroup):
    def __init__(self, client: Client):
        super().__init__("checkouts", client)

    def list(
        self,
        *,
        limit: Optional[int] = None,
        starting_after: Optional[str] = None,
        ending_before: Optional[str] = None,
    ):
        response = self._client.get(
            self._path(),
            params={
                "limit": limit,
                "starting_after": starting_after,
                "ending_before": ending_before,
            },
        )

        return response.json()

    def find(self, checkout_id: str):
        return self._client.get(self._path(checkout_id)).json()

    def delete(self, checkout_id: str):
        return self._client.delete(self._path(checkout_id)).json()

    def create(
        self,
        name: str,
        pricing_type: PricingType,
        local_price: Price,
        *,
        description: str = None,
        requested_info: str = None,
    ):
        payload = {
            "name": name,
            "description": description,
            "pricing_type": pricing_type.name.lower(),
            "local_price": {
                "currency": local_price.currency,
                "amount": local_price.amount,
            },
            "requested_info": [requested_info],
        }

        return self._client.post(self._path(), json=payload).json()

    def update(
        self,
        checkout_id: str,
        name: str,
        pricing_type: PricingType,
        local_price: Price,
        *,
        description: str = None,
    ):
        payload = {
            "name": name,
            "description": description,
            "pricing_type": pricing_type.name.lower(),
            "local_price": {
                "currency": local_price.currency,
                "amount": local_price.amount,
            },
        }

        return self._client.put(self._path(checkout_id), json=payload).json()
